import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3

ApplicationWindow  {
    id: windowRoot
    minimumHeight: 600
    minimumWidth: 600
    modality: Qt.WindowModal
    title: qsTr("DudeDrive")

    signal signalExit

    Material.theme: settings.globalTheme
    Material.accent: settings.globalAccent

    onClosing: {
        //Saving
        accountsDBWindow.hide()
        mainWindow.show()
    }

    Pane {
        anchors.fill: parent

        ListView {
            id: containerSelector

            anchors.fill: parent

            Component {
                id: delegateComponent
                RowLayout {
                    width: parent.width
                    TextField {
                        id: delegateComponentEmail
                        Layout.fillWidth: true
                        text: email
                        placeholderText: "Email"
                    }

                    TextField {
                        id: delegateComponentPassword
                        Layout.fillWidth: true
                        echoMode: TextInput.PasswordEchoOnEdit
                        text: password
                        placeholderText: "Password"
                    }
                }
            }

            model: ListModel {
                ListElement {
                    email: "Dude1"
                    password: "Dude1Pass"
                }
                ListElement {
                    email: "Dude2"
                    password: "Dude2Pass"
                }
                ListElement {
                    email: "Dude2e2e2e2"
                    password: "Dude2Pass"
                }
            }

            delegate: delegateComponent
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
