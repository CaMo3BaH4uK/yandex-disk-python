import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3

ApplicationWindow  {
    id: windowRoot
    width: 300
    height: 200

    minimumHeight: 200
    minimumWidth: 300
    maximumHeight: 200
    maximumWidth: 300

    title: qsTr("DudeDrive")

    signal signalExit

    Pane {
        anchors.fill: parent
        Material.theme: settings.globalTheme
        Material.accent: settings.globalAccent

        Label {
            id: header

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            height: 0.2*parent.height

            font.family: "Helvetica"
            font.pointSize: 16

            text: "Enter disk decryption password"
        }

        ColumnLayout {
            anchors {
                top: header.bottom
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }

            TextField {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: parent.width*0.6
                Layout.preferredHeight: 40
                echoMode: TextInput.Password
                placeholderText: "Password"
            }

            Button {
                Layout.alignment: Qt.AlignCenter
                text: "Decrypt"

                onClicked: {
                }
            }
        }
    }
}
