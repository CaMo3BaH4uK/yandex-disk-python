from PySide2.QtCore import QObject, Slot
from YaDiskClient.YaDiskClient import YaDisk
from tempfile import gettempdir
from os import path, remove
import json
import keyring



class CredentialsProvider(QObject):
    accounts = []
    email = ""
    password = ""
    remembered = False
    autentificated = False


    # try login to YaDisk and download .accounts
    # if the remember == true saves the email and password in keyring
    # return True if authorization was successful
    # else return False
    @Slot(str, str, bool, result=bool)
    def login(self, email, password, remember=False):
        self.email = email
        self.password = password

        try:
            YaDisk(self.email, self.password).download(".accounts", path.join(gettempdir(), ".accounts"))

            if remember:
                
                try:
                    js = json.loads(keyring.get_password("DudeDrive", "accounts"))
                    
                    # If the entered email already exists in keyring
                    if self.email in list(js.keys()):

                        # If the passwords match
                        if self.password != js[self.email]:

                            # ask whether to change the password in json 
                            
                            js[self.email] = self.password
                            print("Password updated\n")

                        else:
                            print("The passwords match\n")

                    else:
                        js[self.email] = self.password
                        

                except:
                    js = {self.email:self.password}
                    print("keyring is Empty\n")

                keyring.set_password("DudeDrive", "accounts", json.dumps(js))
                self.remembered = True

            self.autentificated = True
            self.getAccountsConfig()

            print("Authed as", self.email)
            return True

        except Exception as e:
            print(str(e), "\n")
            return False


    # clears all user data
    @Slot()
    def logout(self):    
        # delete .accounts 
        try:
            remove(path.join(gettempdir(), ".accounts"))
        except:
            print(".accounts not found\n")

        # deleting an account from keyring
        try:
            js = json.loads(keyring.get_password("DudeDrive", "accounts"))
            js.pop(self.email, None)

            if len(js) == 0:
                self.remembered = False

            keyring.set_password("DudeDrive", "accounts", json.dumps(js))

        except:
            print("keyring and so let\n")

        self.email = "" 
        self.password = ""
        self.accounts = []
        self.autentificated = False
        print("logout done\n")
        

    # clear all users
    @Slot()
    def clearKeyring(self):
        self.logout()
        self.remembered = False
        try:
            js = {}
            keyring.set_password("DudeDrive", "accounts", json.dumps(js))
            print("keyring is Empty\n")
        except:
            print("keyring and so let\n")

    
    # return email if authorization was successful
    # else return False
    def checkRememberedAccount(self, email, password):
        if self.remembered:
            try:
                return self.login(email, password)
            except:
                return False


    # if Authenticate == true
    def getAccountsConfig(self):
        # Insert file decoding .accounts 
        ...
        # receive data for authorization
        # Accounts that have successfully passed authorization are write in sefl.accounts
        # If we get an error, we display it and skip the account.
        with open(path.join(gettempdir(), ".accounts"), "r") as f:
            print("Checking accounts: ")
            for string in f.readlines():
                email, password = string.split("\n")[0].split(":")
                
                try:
                    YaDisk(email, password).df()
                    self.accounts.append(YaDisk(email, password))
                except Exception as e:
                    print(email, "   login error: ", str(e), "\n", sep="")
            print("done")

    
    # Displaying email and disk space used
    def getSlaveAccountsDF(self):
        print("Connected accounts:")
        for i in self.accounts:
            df = i.df()
            used = round(int(df['used'])/2**30, 2)
            available = round(int(df['available'])/2**30, 2)

            print(i.login)
            print("used", used, "/", used + available, "Gb\n")


    # return python dict containing in itself Saved Master Accounts
    # email:password
    @Slot(result='QVariant')
    def getSavedMasterAccounts(self):
        js = json.loads(keyring.get_password("DudeDrive", "accounts"))
        return js

    @Slot(result=bool)  
    def savedAccounts(self):
        try:
            return self.getSavedMasterAccounts() != {}
        except:
            return False

    @Slot(result=bool)
    def isAuntificated(self):
        return self.autentificated

    @Slot()
    def silentDeauth(self):
        self.email = ""
        self.password = ""
        self.autentificated = False
        self.remembered = False

    def isRemembered(self):
        return self.remembered
