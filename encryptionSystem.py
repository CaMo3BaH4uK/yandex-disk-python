from Crypto.Cipher import AES
from Crypto.Protocol.KDF import scrypt
from Crypto.Hash import SHA256, MD5
import base64

class encryptionSystem():
    """Encryptor object, encrypts/decrypts files using AES256 (GCM mode), generates encryption key from password.
    """


    password = None
    key = None
    salt = None
    nonce = None

    def __init__(self, password):
        """Init encryptor object.

        Args:
            password (str): a string to generate AES256 encryption key.
        """

        if type(password) != str: # password is not a string
            raise TypeError(password)
        
        try:
            self.password = password.encode('utf-8')
        except: # password is not a utf-8 string
            raise ValueError(password)


        self.salt = self.generateSalt()
        self.nonce = self.generateNonce()
        self.key = self.generateKey()

    
    def generateSalt(self):
        """Generates 32 byte salt for generating 32 byte AES256 encryption key from user password.

        Returns:
            str: 32 byte salt generated from hashes of used password.
        """

        md5Hasher = MD5.new()
        sha256Hasher = SHA256.new()

        md5Hasher.update(self.password)

        passwordMD5Hash = md5Hasher.hexdigest().encode('utf-8')

        sha256Hasher.update(passwordMD5Hash)

        resultSalt = sha256Hasher.hexdigest()[:32][::-1] # some random shit, idk for what and why :D

        return resultSalt

    
    def generateNonce(self):
        """Generated nonce for AES256 (GCM mode) encryption. Not random nonce for every pair of message and password, because we need to easily generate
        nonce from this pair, but using message in generation is not optimized solution, and the nonce is not the most important part of encryption security.

        Returns:
            bytes: nonce for AES256 (GCM mode) ecnryption
        """
        md5Hasher = MD5.new()
        sha256Hasher = MD5.new()

        sha256Hasher.update(self.salt.encode('utf-8'))

        passwordSHA256 = sha256Hasher.hexdigest().encode('utf-8')

        md5Hasher.update(passwordSHA256)

        resultNonce = md5Hasher.hexdigest().encode('utf-8')

        return resultNonce


    def generateKey(self):
        """Generates 32 byte AES256 key for secure data encryption.

        Returns:
            bytes: 32 byte AES256 key.
        """
        # using not random salt to prevent storing it on server, all encryption process must happen locally

        key = scrypt(self.password, self.salt, 32, N=2**18, r=32, p=1)

        return key


    def encryptBytes(self, rawData):
        """Encrypts raw bytes with AES256 (GCM mode).

        Args:
            rawData (bytes): raw bytes that we want to encrypt.

        Returns:
            (bytes, bytes): (encrypted data, mac tag). Mac tag is used to verify data when we want to decrypt it.
        """

        encryptor = AES.new(self.key, AES.MODE_GCM, nonce=self.nonce)

        encryptedBytes, MAC_tag = encryptor.encrypt_and_digest(rawData)

        return (encryptedBytes, MAC_tag)

    def decryptBytes(self, encryptedData, macTag):
        """Decrypts and verifies AES256 (GCM mode) encrypted data.

        Args:
            encryptedData (bytes): raw ecnrypted bytes.
            macTag (bytes): mac tag to verify the file.

        Returns:
            bytes: raw decrypted bytes.
        """
        decryptor = AES.new(self.key, AES.MODE_GCM, nonce=self.nonce)

        try:
            decryptedBytes = decryptor.decrypt_and_verify(encryptedData, macTag)
        except ValueError:
            # default message in case if the file wont pass the mac tag verification
            decryptedBytes = b'THE FILE DIDNT PASS THE VERIFICATION, BECAUSE IT WAS DAMAGED OR COMPROMISED, SO WE CANT TRUST IT.'

        return decryptedBytes


    def readFileBytes(self, fileName):
        """Reads file and returns raw bytes.

        Args:
            fileName (str): name of file that we want to read.

        Returns:
            bytes: raw file bytes.
        """
        with open(fileName, "rb") as rawFile:
            rawBytes = rawFile.read()
        return rawBytes


    def writeFileBytes(self, fileName, rawData):
        """Writes raw bytes to specified file.

        Args:
            fileName (str): name of the specified file.
            rawData (bytes): raw bytes that we want to write into the specified file.
        """
        with open(fileName, "wb") as rawFile:
            rawFile.write(rawData)
        

    def encryptFile(self, fileName):
        """Encrypts file using AES256 (GCM mode).

        Encrypted file stores ecnrypted bytes in the file and mac tag in the filename:.

        Args:
            fileName (str): name of file that we want to encrypt.
        """
        rawFile = self.readFileBytes(fileName)
        rawEncryptedBytes, MAC_tag = self.encryptBytes(rawFile)

        # now we create the final encrypted file structure
        MacTagBase64 = base64.b64encode(MAC_tag)
        rawEncryptedBytesBase64 = base64.b64encode(rawEncryptedBytes)
        finalFileStructure = MacTagBase64 + b' ' + rawEncryptedBytesBase64
        finalFileStructureBase64 = base64.b64encode(finalFileStructure)

        # now we simply write this struct into the file
        self.writeFileBytes(fileName, finalFileStructureBase64)


    def decryptFile(self, fileName):
        """Decrypts file (file is encrypted with AES256).

        Args:
            filename (str): name of file that we want to decrypt.
        """
        
        with open(fileName, 'rb') as encryptedFile:
            # read file and extract the mac tag and ecnrypted data
            fileStructure = base64.b64decode(self.readFileBytes(fileName))
            macTagBase64, encryptedBytesBase64 = map(bytes, fileStructure.split(b' '))
            macTag = base64.b64decode(macTagBase64)
            encryptedBytes = base64.b64decode(encryptedBytesBase64)
        
        # now we simply decrypt and verify the file
        rawData = self.decryptBytes(encryptedBytes, macTag)
        # now we write the result into the file
        self.writeFileBytes(fileName, rawData)
