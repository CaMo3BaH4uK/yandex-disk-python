import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3

ApplicationWindow  {
    id: windowRoot

    minimumHeight: 600
    minimumWidth: 600

    title: qsTr("DudeDrive")

    property var changingAccount: false
    property var forwardToMenu: false

    signal signalExit

    onClosing: {
        Qt.quit()
    }

    data: [Connections {
            target: windowRoot
            onVisibleChanged: {
                var data = credentialsProvider.getSavedMasterAccounts()
                accounts.clear()
                for(var key in data){
                    accounts.append({"email": key})
                }
            }
        }
    ]

    menuBar: MenuBar {
        id: menu
        Material.theme: settings.globalTheme
        Material.accent: settings.globalAccent
        Menu {
            title: "Account"
            Action {
                text: "Add new account"
                onTriggered: {
                    credentialsProvider.silentDeauth()
                    loginWindow.show()
                    accountSelectorWindow.hide()
                    windowRoot.changingAccount = true
                }
            }
            Action {
                text: "Logout from all accounts"
                onTriggered: {
                    credentialsProvider.clearKeyring()
                    loginWindow.show()
                    accountSelectorWindow.hide()
                    windowRoot.changingAccount = true
                }
            }
            Action {
                text: "Quit"
                onTriggered: Qt.quit()
            }
        }
        Menu {
            title: "Theme"
            Action {
                text: "Dark"
                onTriggered: {
                    settings.globalTheme = Material.Dark
                }
            }
            Action {
                text: "Light"
                onTriggered: {
                    settings.globalTheme = Material.Light
                }
            }
        }
    }

    Pane {
        anchors {
            top: menu.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Material.theme: settings.globalTheme
        Material.accent: settings.globalAccent

        ListView {
            id: containerSelector

            anchors.fill: parent

            Component {
                id: delegateComponent
                ItemDelegate {
                    width: parent.width
                    text: email
                    onClicked: {
                        var accountPassword = credentialsProvider.getSavedMasterAccounts()[email]
                        var succesed = credentialsProvider.login(email, accountPassword, true)
                        if (succesed) {
                            forwardToMenu = true
                            mainWindow.show()
                            accountSelectorWindow.hide()
                        }
                    }
                }
            }

            ListModel {
                id: accounts
            }

            model: accounts

            delegate: delegateComponent
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
