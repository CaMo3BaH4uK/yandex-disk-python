# This Python file uses the following encoding: utf-8
import sys
import os

from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine

import provider


if __name__ == "__main__":
    sys.argv += ['--style', 'material']
    app = QGuiApplication(sys.argv)
    app.setOrganizationName("Dudes")
    app.setOrganizationDomain("ethernet.su")
    app.setApplicationName("DudeDrive")
    engine = QQmlApplicationEngine()

    credentialsProvider = provider.CredentialsProvider()

    context = engine.rootContext()
    context.setContextProperty("credentialsProvider", credentialsProvider)

    engine.load(os.path.join(os.path.dirname(__file__), "main.qml"))

    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec_())
