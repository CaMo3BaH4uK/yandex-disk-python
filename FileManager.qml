import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.3

SplitView {
    anchors.fill: parent
    orientation: Qt.Horizontal

    ListView {
        id: containerSelector
        SplitView.preferredWidth: 100
        SplitView.minimumWidth: 100
        SplitView.maximumWidth: parent.width/2

        model: ListModel {
            ListElement {
                name: "Shortcut 1"
            }
            ListElement {
                name: "Shortcut 2"
            }
            ListElement {
                name: "Shortcut 3"
            }
        }

        delegate: ItemDelegate {
            text: name
            width: parent.width
            highlighted: ListView.isCurrentItem
            onClicked: containerSelector.currentIndex = index
        }
    }

    ListView {
        id: filesSelector
        SplitView.fillWidth: true

        model: ListModel {
            ListElement {
                name: "Dir 1"
            }
            ListElement {
                name: "Dir 2"
            }
            ListElement {
                name: "Dir 3"
            }
        }

        delegate: ItemDelegate {
            text: name
            width: parent.width
            highlighted: ListView.isCurrentItem
            onClicked: filesSelector.currentIndex = index
        }

        DropArea {
            id: dropArea;
            anchors.fill: parent
            onDropped: {
                console.log(drop.urls)
            }
        }
    }
}
