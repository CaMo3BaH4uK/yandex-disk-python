import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3

ApplicationWindow  {
    id: windowRoot

    minimumHeight: 600
    minimumWidth: 600

    title: qsTr("DudeDrive")

    signal signalExit

    onClosing: {
        Qt.quit()
    }

    menuBar: MenuBar {
        id: menu
        Material.theme: settings.globalTheme
        Material.accent: settings.globalAccent
        Menu {
            title: "Account"
            Action {
                text: "Quit"
                onTriggered: Qt.quit()
            }
        }
        Menu {
            title: "Theme"
            Action {
                text: "Dark"
                onTriggered: {
                    settings.globalTheme = Material.Dark
                }
            }
            Action {
                text: "Light"
                onTriggered: {
                    settings.globalTheme = Material.Light
                }
            }
        }
    }

    Pane {
        anchors {
            top: menu.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Material.theme: settings.globalTheme
        Material.accent: settings.globalAccent

        Item {
            id: loginPage

            width: 600
            height: 600

            anchors.centerIn: parent

            Label {
                id: header

                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                height: 0.2*parent.height

                font.family: "Helvetica"
                font.pointSize: 24

                text: "DudeDrive"
            }

            ColumnLayout {
                anchors {
                    top: header.bottom
                    bottom: parent.bottom
                    left: parent.left
                    right: parent.right
                }

                TextField {
                    id: email
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: parent.width*0.6
                    Layout.preferredHeight: 40
                    placeholderText: "Email"
                }

                TextField {
                    id: password
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: parent.width*0.6
                    Layout.preferredHeight: 40
                    echoMode: TextInput.Password
                    placeholderText: "Password"
                }

                Switch {
                    id: remember
                    Layout.alignment: Qt.AlignCenter
                    text: "Remember"
                }

                Button {
                    Layout.alignment: Qt.AlignCenter
                    text: "Login"

                    onClicked: {
                        var succesed = credentialsProvider.login(email.text, password.text, remember.position)
                        console.log(succesed)
                        if (succesed == true) {
                            mainWindow.show()
                            loginWindow.hide()
                        }
                    }
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
