import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import Qt.labs.settings 1.1

ApplicationWindow {
    id: root
    minimumHeight: 600
    minimumWidth: 600
    visible: true

    title: qsTr("DudeDrive")

    function selectMode(){
        if (credentialsProvider.savedAccounts()) {
            accountSelectorWindow.show()
            root.close()
        } else {
            loginWindow.show()
            root.close()
        }
    }

    Component.onCompleted: selectMode()

    Settings {
        id: settings

        property var globalTheme
        property var globalAccent
    }

    Pane {
        id: rootBackgroud
        anchors.fill: parent
        Material.theme: settings.globalTheme
        Material.accent: settings.globalAccent

    }

    LoginWindow {
        id: loginWindow
    }

    AccountSelector {
        id: accountSelectorWindow
    }

    MainWindow {
        id: mainWindow
    }

    DecryptDisk {
        id: decryptDiskWindow
    }

    AccountsDBWindow {
        id: accountsDBWindow
    }
}
