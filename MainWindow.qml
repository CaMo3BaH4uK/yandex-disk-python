import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.3

ApplicationWindow  {
    id: windowRoot

    minimumHeight: 600
    minimumWidth: 600

    title: qsTr("DudeDrive")

    signal signalExit

    menuBar: MenuBar {
        id: menu
        Material.theme: settings.globalTheme
        Material.accent: settings.globalAccent
        Menu {
            title: "Account"
            Action {
                text: "Edit accounts DB"
                onTriggered: {
                    accountsDBWindow.show()
                    mainWindow.hide()
                }
            }
            MenuSeparator { }
            Action {
                text: "Select another account"
                onTriggered: {
                    credentialsProvider.silentDeauth()
                    if (credentialsProvider.savedAccounts()) {
                        accountSelectorWindow.show()
                        mainWindow.hide()
                    } else {
                        loginWindow.show()
                        mainWindow.hide()
                    }
                }
            }
            MenuSeparator { }
            Action {
                text: "Logout"
                onTriggered: {
                    credentialsProvider.logout()
                    if (credentialsProvider.savedAccounts()) {
                        accountSelectorWindow.show()
                        mainWindow.hide()
                    } else {
                        loginWindow.show()
                        mainWindow.hide()
                    }
                }
            }
            Action {
                text: "Quit"
                onTriggered: Qt.quit()
            }
        }
        Menu {
            title: "Theme"
            Action {
                text: "Dark"
                onTriggered: {
                    settings.globalTheme = Material.Dark
                }
            }
            Action {
                text: "Light"
                onTriggered: {
                    settings.globalTheme = Material.Light
                }
            }
        }
    }

    Pane {
        anchors {
            top: menu.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Material.theme: settings.globalTheme
        Material.accent: settings.globalAccent

        FileManager {

        }

    }
}
